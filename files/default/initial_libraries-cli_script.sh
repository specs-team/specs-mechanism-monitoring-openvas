#!/bin/bash

echo "Starting script per le libraries-cli..."

PATH_FILE_BASH=/root/.bashrc
PATH_FILE_BASH_LD_LIBRARY=/etc/bash.bashrc

echo "1 - Set PATH variable"
echo export "PATH=\$PATH\":/opt/openvas-libraries-cli/bin/:/opt/openvas-xml-utilities/xmlstarlet/bin\"" >> $PATH_FILE_BASH

echo "2 - Set LD_LIBRARY_PATH variable"
echo export "LD_LIBRARY_PATH=\$LD_LIBRARY_PATH\":/opt/openvas-libs/libgcrypt/lib64/:/opt/openvas-libs/libgpg-error/lib64/:/opt/openvas-libraries-cli/lib/:/opt/openvas-libs/gnutls/lib64/:/opt/openvas-libs/gpgme/lib64/:/opt/openvas-libs/libpcap/lib64/\"" >> $PATH_FILE_BASH_LD_LIBRARY

echo "Script finished!"
