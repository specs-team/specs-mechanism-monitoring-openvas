specs-monitoring-openvas Cookbook
====================
This cookbook contains two recipes: the "manager" one allows to install the Openvas scanner and Openvas manager on the target machine, while the "client" recipe  allows to install the 
Openvas client with omp command line tool, and a custom java program which start and manage the vulnerability scanning and send the result to the specs-monitoring-core. 

Requirements
------------
1) The manager recipe has to be executed on all the target nodes before the 
execution of the client.

2) The manager:  the target machine needs the tcp port 9390 to be 
opened. On Amazon AMI: you need to:
- access to amazon console;
- add an inbound rule into the security group [Custom TCP rule].   
  
4) The target machine need the OpenSuse OS.

Attributes
----------
For each recipe,The attributes have to be passed a implementation plan id : 

{"implementation_plan_id":"ID_VALUE"}

Usage                              
-----------

#### create implementation plans data bag
if not exist, create a data bag "implementation_plans"  with follow command: knife data bag create implementation_plans  

#### add implementation plan
add a item: knife data bag from file implementation_plan.json 

  example of implementation_plan that provide one node with openvas-monitoring-client and one openvas-monitoring-manager:
 
```
{
  "context": {
    "slaID": "$SLA-ID",
    "planID": "$PLAN-ID",
    "annotations": [],
    "monitoring-core-ip": "192.168.1.150",
    "monitoring-core-port": "5000"
  },
  "IaaS": {
    "VMs": {
      "nodes-access-credentials-reference": "$$ filled by broker or $by plan",
      "nodes": [
        {
          "node-id": "client",
          "creation-time": "$$ filled by broker",
          "annotations": [],
          "provider": "aws-east",
          "appliance": "us-east-1/ami-ff0e0696",
          "hardware": "t1.micro",
          "storages": [],
          "acquire-public-ip": true,
          "private-ips-count": 1,
          "firewall": {
            "incoming": [
              {
                "source-ips": "0.0.0.0/0",
                "source-nodes": [],
                "interface": "public",
                "proto": "TCP",
                "port-list": "22"
              }
            ],
            "outcoming": [
              {
                "destination-ips": "0.0.0.0/0",
                "destination-nodes": [],
                "interface": "public,private:1",
                "proto": "TCP",
                "port-list": "*"
              }
            ]
          },
          "recipes": [
            {
              "id": "specs-recipe-2",
              "cookbook": "specs-monitoring-openvas",
              "name": "client",
              "implementation-phase": 1
            }
          ],
          "private-ips": [
            "172.31.54.179"
          ],
          "public-ip": "52.1.49.101"
        },
        {
          "node-id": "scanner1",
          "creation-time": "$$ filled by broker",
          "annotations": [],
          "provider": "aws-east",
          "appliance": "us-east-1/ami-ff0e0696",
          "hardware": "t1.micro",
          "storages": [],
          "acquire-public-ip": false,
          "private-ips-count": 1,
          "firewall": {
            "incoming": [
              {
                "source-ips": "",
                "source-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "22,9390"
              }
            ],
            "outcoming": [
              {
                "destination-ips": "",
                "destination-nodes": [
                  "ha-proxy"
                ],
                "interface": "private:1",
                "proto": "TCP",
                "port-list": "*"
              }
            ]
          },
          "recipes": [
            {
              "id": "specs-recipe-3",
              "cookbook": "specs-monitoring-openvas",
              "name": "manager",
              "implementation-phase": 1
            }
          ],
          "private-ips": [
            "172.31.54.51"
          ],
          "public-ip": "52.5.35.191"
        }
      ]
    },
    "STORAGEs": []
  },
  "providers": [
    {
      "id": "aws-east",
      "name": "aws-ec2",
      "zone": "us-east-1"
    }
  ]
}
   
```

#### specs-monitoring-openvas::manager

knife bootstrap <IP_PUBLIC> -x <user_name> -P <user_password> --node-name <node_name> --run-list 'recipe['specs-monitoring-openvas::manager']' -j '{ 
"implementation_plan_id":"<id_value>"}' 

example : knife bootstrap 192.168.1.101 -x root -P specs --node-name node1 --run-list 'recipe['specs-monitoring-openvas::manager']' -j '{ "implementation_plan_id":"1154982"}'
          knife bootstrap 192.168.1.103 -x root -P specs --node-name node2 --run-list 'recipe['specs-monitoring-openvas::maager']' -j '{ "implementation_plan_id":"1154982"}'

#### specs-monitoring-openvas::client

knife bootstrap <IP_PUBLIC> -x <user_name> -P <user_password> --node-name <node_name> --run-list 'recipe['specs-monitoring-openvas::client']' -j '{ '{ 
"implementation_plan_id":"<id_value>"}'}'

example : knife bootstrap 192.168.1.103 -x root -P specs --node-name server-node --run-list 'recipe['specs-monitoring-openvas::client']' -j '{ "implementation_plan_id":"1154982"}'

 

Contributors
------------------
Vincenzo Cinque & Pasquale De Rosa


License and Authors
-------------------
Authors: Cinque Vincenzo & Capone Giancarlo.
License :
//# Copyright (C) 2014 - 2016 CeRICT
//# Developed for the SPECS Project FP7 - GA 610795 - 
//# Authors: 
//# %%
//# Licensed under the Apache License, Version 2.0 (the "License");
//# you may not use this file except in compliance with the License.
//# You may obtain a copy of the License at
//# 
//#      http://www.apache.org/licenses/LICENSE-2.0
//# 
//# Unless required by applicable law or agreed to in writing, software
//# distributed under the License is distributed on an "AS IS" BASIS,
//# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
implied.
//# See the License for the specific language governing permissions and
//# limitations under the License.

