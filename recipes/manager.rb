#
# Cookbook Name:: specs-monitoring-openvas
# Recipe:: manager
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# Download package scanner-manager

cookbook_file "openvas-libs-libraries-scanner-manager.tar.gz" do
   path "/tmp/openvas-libs-libraries-scanner-manager.tar.gz"
   action :create
end

# Download installer script

cookbook_file "initial_libraries-scanner-manager_script.sh" do
   path "/tmp/initial_libraries-scanner-manager_script.sh"
   action :create
end

# unzip tar file & execute installer script

bash "install_program" do
        user "root"
        cwd  "/opt"
        code <<-EOH
        tar -zxvf /tmp/openvas-libs-libraries-scanner-manager.tar.gz
	rm /tmp/openvas-libs-libraries-scanner-manager.tar.gz
	chmod +x /tmp/initial_libraries-scanner-manager_script.sh 
	/tmp/initial_libraries-scanner-manager_script.sh userAdmin passAdmin > install.log
        rm /tmp/initial_libraries-scanner-manager_script.sh
        EOH
end
 



